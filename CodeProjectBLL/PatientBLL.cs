﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using DataModels;
using CodeProjectBLL;

namespace CodeProjectBLL
{
    /// <summary>
    /// Patient Busines Logic Layer
    /// </summary>
    public class PatientBLL
    {

        /// <summary>
        /// Get Patient Information
        /// </summary>
        /// <param name="patientInformation"></param>
        /// <param name="returnStatus"></param>
        /// <param name="returnMessage"></param>
        /// <returns></returns>
        public Patient GetPatientInformation(long patientID, 
            out bool returnStatus, 
            out string returnErrorMessage,
            out List<string> returnMessages)
        {
            
            try
            {
          
                StringBuilder sqlBuilder = new StringBuilder();

                string sqlString = "SELECT * FROM PATIENT WHERE PATIENT_ID = @PATIENT_ID";
    
                SqlConnection connection = CreateConnection(out returnStatus, out returnErrorMessage);

                SqlCommand patientCommand = new SqlCommand();
                patientCommand.CommandType = CommandType.Text;
                patientCommand.Connection = connection;
                patientCommand.CommandText = sqlString;

                SqlParameter param1 = new SqlParameter("@PATIENT_ID", SqlDbType.VarChar);
                param1.Value = patientID;
                patientCommand.Parameters.Add(param1);

                SqlDataAdapter sqlAdapter = new SqlDataAdapter(patientCommand);

                DataSet patientData = new DataSet();
                sqlAdapter.Fill(patientData, "Patients");

                connection.Close();
             
                List<string> outputMessages = new List<string>();
                outputMessages.Add("Patient Information Retrieved");

                returnStatus = true;
                returnMessages = outputMessages;

                Patient patient = new Patient();

                patient.PatientID = Convert.ToInt64(patientData.Tables[0].Rows[0]["Patient_ID"]);
                patient.MedicalID = UtilitiesBLL.ConvertToString(patientData.Tables[0].Rows[0]["Medical_ID"]);
                patient.SocialSecurityNumber = UtilitiesBLL.ConvertToString(patientData.Tables[0].Rows[0]["Social_Security_Number"]);
                patient.FirstName = UtilitiesBLL.ConvertToString(patientData.Tables[0].Rows[0]["First_Name"]);
                patient.LastName = UtilitiesBLL.ConvertToString(patientData.Tables[0].Rows[0]["Last_Name"]);           
                patient.AddressLine1 =  UtilitiesBLL.ConvertToString(patientData.Tables[0].Rows[0]["Address_Line1"]);
                patient.AddressLine2 = UtilitiesBLL.ConvertToString(patientData.Tables[0].Rows[0]["Address_Line2"]);
                patient.City = UtilitiesBLL.ConvertToString(patientData.Tables[0].Rows[0]["City"]);
                patient.State = UtilitiesBLL.ConvertToString(patientData.Tables[0].Rows[0]["State"]);
                patient.ZipCode = UtilitiesBLL.ConvertToString(patientData.Tables[0].Rows[0]["Zip_Code"]);
                patient.PhoneNumber = UtilitiesBLL.ConvertToString(patientData.Tables[0].Rows[0]["Phone_Number"]);

                DateTime dateResult;
                string dateOfBirth = UtilitiesBLL.ConvertToString(patientData.Tables[0].Rows[0]["Date_Of_Birth"]);

                patient.PatientDateOfBirth = dateOfBirth;

                if (DateTime.TryParse(dateOfBirth,out dateResult)==true)            
                    patient.DateOfBirth = Convert.ToDateTime(patientData.Tables[0].Rows[0]["Date_Of_Birth"]);
                
                string effectiveDate = UtilitiesBLL.ConvertToString(patientData.Tables[0].Rows[0]["Effective_Date"]);

                patient.PatientEffectiveDate = effectiveDate;

                if (DateTime.TryParse(effectiveDate, out dateResult) == true)
                    patient.EffectiveDate = Convert.ToDateTime(patientData.Tables[0].Rows[0]["Effective_Date"]);

                return patient;

            }
            catch (Exception ex)
            {
 
                List<string> outputMessages = new List<string>();              

                returnStatus = false;
                returnErrorMessage = ex.Message;
                returnMessages = outputMessages;

                Patient patient = new Patient();

                return patient;

            }

        }
        
        /// <summary>
        /// Check For Duplicate Patient
        /// </summary>
        /// <param name="patientID"></param>
        /// <param name="medicalID"></param>
        /// <param name="socialSecurityNumber"></param>
        /// <returns></returns>
        public bool DuplicatePatient(long patientID, 
            string medicalID, 
            string socialSecurityNumber, 
            out bool returnStatus, 
            out string returnErrorMessage)
        {

            try
            {

                StringBuilder sqlBuilder = new StringBuilder();

                sqlBuilder.Append("SELECT PATIENT_ID FROM PATIENT ");
                sqlBuilder.Append(" WHERE ( MEDICAL_ID = @MEDICAL_ID ");
                sqlBuilder.Append("    OR SOCIAL_SECURITY_NUMBER = @SOCIAL_SECURITY_NUMBER ) ");

                if (patientID > 0)
                {
                    sqlBuilder.Append(" AND PATIENT_ID <> @PATIENT_ID");
                }

                string sqlString = sqlBuilder.ToString();

                SqlConnection connection = CreateConnection(out returnStatus, out returnErrorMessage);

                SqlCommand patientCommand = new SqlCommand();
                patientCommand.CommandType = CommandType.Text;
                patientCommand.Connection = connection;
                patientCommand.CommandText = sqlString;

                SqlParameter param1 = new SqlParameter("@MEDICAL_ID", SqlDbType.VarChar);
                param1.Value = medicalID;
                patientCommand.Parameters.Add(param1);

                SqlParameter param2 = new SqlParameter("@SOCIAL_SECURITY_NUMBER", SqlDbType.VarChar);
                param2.Value = socialSecurityNumber;
                patientCommand.Parameters.Add(param2);

                if (patientID > 0)
                {
                    SqlParameter param3 = new SqlParameter("@PATIENT_ID", SqlDbType.BigInt);
                    param3.Value = patientID;
                    patientCommand.Parameters.Add(param3);
                }

                bool duplicatePatient = false;

                IDataReader dataReader = patientCommand.ExecuteReader();
                if (dataReader.Read())
                {
                    duplicatePatient = true;
                }

                connection.Close();

                returnStatus = true;
                returnErrorMessage = "";

                return duplicatePatient;

            }
            catch (Exception ex)
            {               
                returnStatus = false;
                returnErrorMessage = ex.Message;

                return false;

            }

        }

        /// <summary>
        /// Format Patient Data
        /// </summary>
        /// <param name="patientInformation"></param>
        public void FormatPatientData(ref Patient patientInformation, 
            out bool returnStatus,                                    
            out string returnErrorMessage)
        {

            try
            {

                if (patientInformation.MedicalID == null) patientInformation.MedicalID = "";
                if (patientInformation.SocialSecurityNumber == null) patientInformation.SocialSecurityNumber = "";
                if (patientInformation.FirstName == null) patientInformation.FirstName = "";
                if (patientInformation.LastName == null) patientInformation.LastName = "";
                if (patientInformation.AddressLine1 == null) patientInformation.AddressLine1 = "";
                if (patientInformation.AddressLine2 == null) patientInformation.AddressLine2 = "";
                if (patientInformation.City == null) patientInformation.City = "";
                if (patientInformation.State == null) patientInformation.State = "";
                if (patientInformation.ZipCode == null) patientInformation.ZipCode = "";
                if (patientInformation.PhoneNumber == null) patientInformation.PhoneNumber = "";

                patientInformation.MedicalID = patientInformation.MedicalID.ToUpper().Trim();
                patientInformation.SocialSecurityNumber = patientInformation.SocialSecurityNumber.ToUpper().Trim();
                patientInformation.FirstName = patientInformation.FirstName.ToUpper().Trim();
                patientInformation.LastName = patientInformation.LastName.ToUpper().Trim();
                patientInformation.AddressLine1 = patientInformation.AddressLine1.ToUpper().Trim();
                patientInformation.AddressLine2 = patientInformation.AddressLine2.ToUpper().Trim();
                patientInformation.City = patientInformation.City.ToUpper().Trim();
                patientInformation.State = patientInformation.State.ToUpper().Trim();
                patientInformation.ZipCode = patientInformation.ZipCode.ToUpper().Trim();
                patientInformation.PhoneNumber = patientInformation.PhoneNumber.ToUpper().Trim();

                returnErrorMessage = "";
                returnStatus = true;

            }
            catch (Exception ex)
            {
                returnErrorMessage = ex.Message;
                returnStatus = false;
            }

        }

        /// <summary>
        /// Validate Patient
        /// </summary>
        /// <param name="patientInformation"></param>
        /// <returns></returns>
        public bool ValidatePatient(Patient patientInformation, 
            out List<string> Messages, 
            out bool returnStatus, 
            out string returnErrorMessage)
        {

            try
            {

                bool validPatient = true;

                List<string> outputMessages = new List<string>();

                if (patientInformation.MedicalID == null || patientInformation.MedicalID.Trim().Length == 0)
                {
                    outputMessages.Add("Medical ID is required");
                    validPatient = false;
                }

                if (patientInformation.SocialSecurityNumber == null || patientInformation.SocialSecurityNumber.Trim().Length == 0)
                {
                    outputMessages.Add("Social Security Number is required");
                    validPatient = false;
                }

                if (patientInformation.FirstName == null || patientInformation.FirstName.Trim().Length == 0)
                {
                    outputMessages.Add("First Name is required");
                    validPatient = false;
                }

                if (patientInformation.LastName == null || patientInformation.LastName.Trim().Length == 0)
                {
                    outputMessages.Add("Last Name is required");
                    validPatient = false;
                }

                if (CodeProjectBLL.UtilitiesBLL.IsValidDate(patientInformation.PatientDateOfBirth) == false)
                {
                    outputMessages.Add("Date Of Birth is invalid");
                    validPatient = false;
                }

                if (CodeProjectBLL.UtilitiesBLL.IsValidDate(patientInformation.PatientEffectiveDate) == false)
                {
                    outputMessages.Add("Effective Date is invalid");
                    validPatient = false;
                }

                if (CodeProjectBLL.UtilitiesBLL.IsDateSupplied(patientInformation.PatientEffectiveDate) == false)
                {
                    outputMessages.Add("Effective Date is required.");
                    validPatient = false;
                }

                if (patientInformation.MedicalID != null && patientInformation.SocialSecurityNumber != null)
                {

                    bool duplicatePatient = DuplicatePatient(
                        patientInformation.PatientID,
                        patientInformation.MedicalID,
                        patientInformation.SocialSecurityNumber, 
                        out returnStatus, 
                        out returnErrorMessage);

                    if (returnStatus == false)
                    {
                        outputMessages.Add(returnErrorMessage);
                        Messages = outputMessages;
                        return false;
                    }

                    if (duplicatePatient == true)
                    {
                        outputMessages.Add("Duplicate Medical ID and/or Social Security Number");
                        validPatient = false;
                    }
                }

                Messages = outputMessages;

                returnStatus = true;
                returnErrorMessage = "";

                return validPatient;
            }
            catch (Exception ex)
            {
                List<string> outputMessages = new List<string>();
                Messages = outputMessages;

                returnStatus = false;
                returnErrorMessage = ex.Message;
                
                return false;
            }

        }
        
        /// <summary>
        /// Add Patient
        /// </summary>
        /// <param name="patientInformation"></param>
        /// <param name="returnStatus"></param>
        /// <param name="returnMessage"></param>
        /// <returns></returns>
        public Patient AddPatient(Patient patientInformation,              
            out List<string>returnMessages, 
            out bool returnStatus,
            out string returnErrorMessage)
        {
            try
            {
                StringBuilder sqlBuilder = new StringBuilder();

                List<string> messages;

                FormatPatientData(ref patientInformation, out returnStatus, out returnErrorMessage);

                bool validPatient = ValidatePatient(patientInformation, out messages, out returnStatus, out returnErrorMessage);
                if (validPatient == false)
                {
                    returnStatus = false;
                    returnMessages = messages;
                    return patientInformation;
                }

                sqlBuilder.Append("INSERT INTO PATIENT ( ");

                sqlBuilder.Append(" MEDICAL_ID,SOCIAL_SECURITY_NUMBER,FIRST_NAME,LAST_NAME,");
                sqlBuilder.Append(" ADDRESS_LINE1,ADDRESS_LINE2,CITY,STATE,ZIP_CODE,");
                sqlBuilder.Append(" PHONE_NUMBER,DATE_OF_BIRTH,EFFECTIVE_DATE) VALUES (");

                sqlBuilder.Append(" @MEDICAL_ID,@SOCIAL_SECURITY_NUMBER,@FIRST_NAME,@LAST_NAME,");
                sqlBuilder.Append(" @ADDRESS_LINE1,@ADDRESS_LINE2,@CITY,@STATE,@ZIP_CODE,");
                sqlBuilder.Append(" @PHONE_NUMBER,@DATE_OF_BIRTH,@EFFECTIVE_DATE)");

                string sqlString = sqlBuilder.ToString();

                SqlConnection connection = CreateConnection(out returnStatus, out returnErrorMessage);

                SqlCommand patientCommand = new SqlCommand();
                patientCommand.CommandType = CommandType.Text;
                patientCommand.Connection = connection;
                patientCommand.CommandText = sqlString;

                SqlParameter param1 = new SqlParameter("@MEDICAL_ID", SqlDbType.VarChar);
                param1.Value = patientInformation.MedicalID;
                patientCommand.Parameters.Add(param1);

                SqlParameter param2 = new SqlParameter("@SOCIAL_SECURITY_NUMBER", SqlDbType.VarChar);
                param2.Value = patientInformation.SocialSecurityNumber;
                patientCommand.Parameters.Add(param2);

                SqlParameter param3 = new SqlParameter("@FIRST_NAME", SqlDbType.VarChar);
                param3.Value = patientInformation.FirstName;
                patientCommand.Parameters.Add(param3);

                SqlParameter param4 = new SqlParameter("@LAST_NAME", SqlDbType.VarChar);
                param4.Value = patientInformation.LastName;
                patientCommand.Parameters.Add(param4);

                SqlParameter param5 = new SqlParameter("@ADDRESS_LINE1", SqlDbType.VarChar);
                param5.Value = patientInformation.AddressLine1.ToUpper().Trim();
                patientCommand.Parameters.Add(param5);

                SqlParameter param6 = new SqlParameter("@ADDRESS_LINE2", SqlDbType.VarChar);
                param6.Value = patientInformation.AddressLine2;
                patientCommand.Parameters.Add(param6);

                SqlParameter param7 = new SqlParameter("@CITY", SqlDbType.VarChar);
                param7.Value = patientInformation.City;
                patientCommand.Parameters.Add(param7);

                SqlParameter param8 = new SqlParameter("@STATE", SqlDbType.VarChar);
                param8.Value = patientInformation.State;
                patientCommand.Parameters.Add(param8);

                SqlParameter param9 = new SqlParameter("@ZIP_CODE", SqlDbType.VarChar);
                param9.Value = patientInformation.ZipCode;
                patientCommand.Parameters.Add(param9);

                SqlParameter param10 = new SqlParameter("@PHONE_NUMBER", SqlDbType.VarChar);
                param10.Value = patientInformation.PhoneNumber;
                patientCommand.Parameters.Add(param10);

                SqlParameter param11 = new SqlParameter("@DATE_OF_BIRTH", SqlDbType.Date);
                param11.Value = patientInformation.DateOfBirth;
                patientCommand.Parameters.Add(param11);

                SqlParameter param12 = new SqlParameter("@EFFECTIVE_DATE", SqlDbType.Date);
                param12.Value = patientInformation.EffectiveDate;
                patientCommand.Parameters.Add(param12);

                patientCommand.ExecuteNonQuery();

                sqlString = "SELECT PATIENT_ID FROM PATIENT WHERE MEDICAL_ID = @MEDICAL_ID";

                SqlCommand patientIDCommand = new SqlCommand();
                patientIDCommand.CommandType = CommandType.Text;
                patientIDCommand.Connection = connection;
                patientIDCommand.CommandText = sqlString;

                SqlParameter medicalIDParam = new SqlParameter("@MEDICAL_ID", SqlDbType.VarChar);
                medicalIDParam.Value = patientInformation.MedicalID.ToUpper().Trim();
                patientIDCommand.Parameters.Add(medicalIDParam);

                patientInformation.PatientID = Convert.ToInt64(patientIDCommand.ExecuteScalar());

                connection.Close();

                List<string> outputMessages = new List<string>();
                outputMessages.Add("Patient has been added.");

                returnStatus = true;
                returnErrorMessage = "";
                returnMessages = outputMessages;

                return patientInformation;
            }
            catch (Exception ex)
            {
                List<string> outputMessages = new List<string>();
                outputMessages.Add(ex.Message);

                returnStatus = false;
                returnErrorMessage = ex.Message;
                returnMessages = outputMessages;

                return patientInformation;
            }
        }

        /// <summary>
        /// Update Patient
        /// </summary>
        /// <param name="patientInformation"></param>
        /// <param name="returnStatus"></param>
        /// <param name="returnMessage"></param>
        /// <returns></returns>
        public Patient UpdatePatient(Patient patientInformation, 
            out bool returnStatus,          
            out List<string> returnMessages)
        {

            try
            {

                StringBuilder sqlBuilder = new StringBuilder();

                List<string> messages;

                string returnErrorMessage;

                FormatPatientData(ref patientInformation, out returnStatus, out returnErrorMessage);

                bool validPatient = ValidatePatient(
                    patientInformation,
                    out messages,
                    out returnStatus,
                    out returnErrorMessage);

                if (returnStatus == false)
                {
                    returnStatus = false;
                    messages.Add(returnErrorMessage);
                    returnMessages = messages;
                    return patientInformation;
                }

                if (validPatient == false)
                {
                    returnStatus = false;                   
                    returnMessages = messages;
                    return patientInformation;
                }

                sqlBuilder.Append("UPDATE PATIENT SET ");
                sqlBuilder.Append(" MEDICAL_ID = @MEDICAL_ID,");
                sqlBuilder.Append(" SOCIAL_SECURITY_NUMBER = @SOCIAL_SECURITY_NUMBER,");
                sqlBuilder.Append(" FIRST_NAME = @FIRST_NAME,");
                sqlBuilder.Append(" LAST_NAME = @LAST_NAME,");
                sqlBuilder.Append(" ADDRESS_LINE1 = @ADDRESS_LINE1,");
                sqlBuilder.Append(" ADDRESS_LINE2 = @ADDRESS_LINE2,");
                sqlBuilder.Append(" CITY = @CITY,");
                sqlBuilder.Append(" STATE = @STATE,");
                sqlBuilder.Append(" ZIP_CODE = @ZIP_CODE,");
                sqlBuilder.Append(" PHONE_NUMBER = @PHONE_NUMBER, ");
                sqlBuilder.Append(" DATE_OF_BIRTH = @DATE_OF_BIRTH, ");
                sqlBuilder.Append(" EFFECTIVE_DATE = @EFFECTIVE_DATE ");
                sqlBuilder.Append(" WHERE PATIENT_ID = @PATIENT_ID ");

                string sqlString = sqlBuilder.ToString();

                SqlConnection connection = CreateConnection(out returnStatus, out returnErrorMessage);

                SqlCommand patientCommand = new SqlCommand();
                patientCommand.CommandType = CommandType.Text;
                patientCommand.Connection = connection;
                patientCommand.CommandText = sqlString;

                SqlParameter paramPatientID = new SqlParameter("@PATIENT_ID", SqlDbType.BigInt);
                paramPatientID.Value = patientInformation.PatientID;
                patientCommand.Parameters.Add(paramPatientID);

                SqlParameter param1 = new SqlParameter("@MEDICAL_ID", SqlDbType.VarChar);
                param1.Value = patientInformation.MedicalID;
                patientCommand.Parameters.Add(param1);

                SqlParameter param2 = new SqlParameter("@SOCIAL_SECURITY_NUMBER", SqlDbType.VarChar);
                param2.Value = patientInformation.SocialSecurityNumber;
                patientCommand.Parameters.Add(param2);

                SqlParameter param3 = new SqlParameter("@FIRST_NAME", SqlDbType.VarChar);
                param3.Value = patientInformation.FirstName;
                patientCommand.Parameters.Add(param3);

                SqlParameter param4 = new SqlParameter("@LAST_NAME", SqlDbType.VarChar);
                param4.Value = patientInformation.LastName;
                patientCommand.Parameters.Add(param4);

                SqlParameter param5 = new SqlParameter("@ADDRESS_LINE1", SqlDbType.VarChar);
                param5.Value = patientInformation.AddressLine1;
                patientCommand.Parameters.Add(param5);

                SqlParameter param6 = new SqlParameter("@ADDRESS_LINE2", SqlDbType.VarChar);
                param6.Value = patientInformation.AddressLine2;
                patientCommand.Parameters.Add(param6);

                SqlParameter param7 = new SqlParameter("@CITY", SqlDbType.VarChar);
                param7.Value = patientInformation.City;
                patientCommand.Parameters.Add(param7);

                SqlParameter param8 = new SqlParameter("@STATE", SqlDbType.VarChar);
                param8.Value = patientInformation.State;
                patientCommand.Parameters.Add(param8);

                SqlParameter param9 = new SqlParameter("@ZIP_CODE", SqlDbType.VarChar);
                param9.Value = patientInformation.ZipCode;
                patientCommand.Parameters.Add(param9);

                SqlParameter param10 = new SqlParameter("@PHONE_NUMBER", SqlDbType.VarChar);
                param10.Value = patientInformation.PhoneNumber;
                patientCommand.Parameters.Add(param10);

                SqlParameter param11 = new SqlParameter("@DATE_OF_BIRTH", SqlDbType.Date);
                param11.Value = patientInformation.DateOfBirth;
                patientCommand.Parameters.Add(param11);

                SqlParameter param12 = new SqlParameter("@EFFECTIVE_DATE", SqlDbType.Date);
                param12.Value = patientInformation.EffectiveDate;
                patientCommand.Parameters.Add(param12);

                patientCommand.ExecuteNonQuery();

                connection.Close();

                List<string> outputMessages = new List<string>();
                outputMessages.Add("Patient has been updated.");

                returnStatus = true;
                returnErrorMessage = "";
                returnMessages = outputMessages;

                return patientInformation;
            }
            catch (Exception ex)
            {
                List<string> outputMessages = new List<string>();
 
                returnStatus = false;
                outputMessages.Add(ex.Message);
                returnMessages = outputMessages;

                return patientInformation;

            }

        }

        /// <summary>
        /// Search For Patients
        /// </summary>
        /// <returns></returns>
        public List<Patient> PatientSearch(
            Patient patientSearchValues, 
            long currentPageNumber, 
            long pageSize, 
            string sortBy, 
            string sortAscendingDescending,
            out long totalRows, 
            out long totalPages,
            out bool returnStatus,
            out string returnErrorMessage)
        {
       
            long pageNumber;
            long currentRow;
            long result;

            try
            {
                totalPages = 0;
                totalRows = 0;

                List<Patient> patients = new List<Patient>();

                DataTable patientDataTable = GetPatients(patientSearchValues, 
                    sortBy, 
                    sortAscendingDescending, 
                    out returnStatus, 
                    out returnErrorMessage);

                if (returnStatus == false)
                {                                        
                    return patients;
                }

                totalRows = patientDataTable.Rows.Count;
                totalPages = 0;

                Math.DivRem(totalRows, pageSize, out result);
                if (result > 0)
                    totalPages = Convert.ToInt64(totalRows / pageSize) + 1;
                else
                    totalPages = Convert.ToInt64(totalRows / pageSize);

                currentRow = 0;
                pageNumber = 1;

                for (int i = 0; i < patientDataTable.Rows.Count; i++)
                {

                    currentRow++;

                    if (currentRow > pageSize)
                    {
                        pageNumber++;
                        currentRow = 1;
                    }

                    if (currentPageNumber == pageNumber)
                    {
                        Patient patientList = new Patient();

                        patientList.PatientID = Convert.ToInt64(patientDataTable.Rows[i]["Patient_ID"]);
                        patientList.FirstName = patientDataTable.Rows[i]["First_Name"].ToString();
                        patientList.LastName = patientDataTable.Rows[i]["Last_Name"].ToString();
                        patientList.MedicalID = patientDataTable.Rows[i]["Medical_ID"].ToString();
                        patientList.SocialSecurityNumber = patientDataTable.Rows[i]["Social_Security_Number"].ToString();

                        patients.Add(patientList);

                        if (patients.Count == pageSize)
                        {
                            returnErrorMessage = "";
                            returnStatus = true;
                            return patients;
                        }

                    }
                }

                returnErrorMessage = "";
                returnStatus = true;
                
                return patients;

            }
            catch (Exception ex)
            {
                returnErrorMessage = ex.Message;
                returnStatus = false;
                totalPages = 0;
                totalRows = 0;

                List<Patient> patients = new List<Patient>();

                return patients;
            }

        }

       /// <summary>
       /// Get Patients
       /// </summary>
       /// <param name="patientSearchValues"></param>
       /// <param name="sortBy"></param>
       /// <param name="sortAscendingDescending"></param>
       /// <param name="returnStatus"></param>
       /// <param name="returnErrorMessage"></param>
       /// <returns></returns>
        private DataTable GetPatients(
            Patient patientSearchValues,
            string sortBy,
            string sortAscendingDescending,
            out bool returnStatus,
            out string returnErrorMessage)
        {

            try
            {

                StringBuilder sqlBuilder = new StringBuilder();
                StringBuilder sqlWhereClause = new StringBuilder();

                DataSet patientData = new DataSet();

                string sqlWhereString;

                String sqlString = sqlBuilder.ToString();

                SqlConnection connection = CreateConnection(out returnStatus, out returnErrorMessage);
                if (returnStatus == false) return null;
            
                SqlCommand patientCommand = new SqlCommand();
                patientCommand.CommandType = CommandType.Text;
                patientCommand.Connection = connection;

                sqlBuilder.Append("SELECT * FROM PATIENT ");

                if (patientSearchValues.FirstName != null)
                {
                    sqlWhereClause.Append(" FIRST_NAME LIKE @FIRST_NAME AND ");
                    SqlParameter param1 = new SqlParameter("@FIRST_NAME", SqlDbType.VarChar);
                    param1.Value = patientSearchValues.FirstName.ToUpper().Trim() + "%";
                    patientCommand.Parameters.Add(param1);
                }
                if (patientSearchValues.LastName != null)
                {
                    sqlWhereClause.Append(" LAST_NAME LIKE @LAST_NAME AND ");
                    SqlParameter param2 = new SqlParameter("@LAST_NAME", SqlDbType.VarChar);
                    param2.Value = patientSearchValues.LastName.ToUpper().Trim() + "%";
                    patientCommand.Parameters.Add(param2);
                }
                if (patientSearchValues.MedicalID != null)
                {
                    sqlWhereClause.Append(" MEDICAL_ID = @MEDICAL_ID AND ");
                    SqlParameter param3 = new SqlParameter("@MEDICAL_ID", SqlDbType.VarChar);
                    param3.Value = patientSearchValues.MedicalID.ToUpper().Trim();
                    patientCommand.Parameters.Add(param3);
                }
                if (patientSearchValues.SocialSecurityNumber != null)
                {
                    sqlWhereClause.Append(" SOCIAL_SECURITY_NUMBER = @SOCIAL_SECURITY_NUMBER AND ");
                    SqlParameter param4 = new SqlParameter("@SOCIAL_SECURITY_NUMBER", SqlDbType.VarChar);
                    param4.Value = patientSearchValues.SocialSecurityNumber.Trim();
                    patientCommand.Parameters.Add(param4);
                }

                if (sqlWhereClause.Length > 0)
                {
                    sqlWhereString = sqlWhereClause.ToString().Substring(0, sqlWhereClause.Length - 5);
                    sqlBuilder.Append(" WHERE " + sqlWhereString);
                }

                if (sortBy=="LastName")
                    sqlBuilder.Append(" ORDER BY LAST_NAME ");
                else if (sortBy=="FirstName")
                    sqlBuilder.Append(" ORDER BY FIRST_NAME ");
                else if (sortBy=="MedicalID")
                    sqlBuilder.Append(" ORDER BY MEDICAL_ID ");
                else if (sortBy=="SocialSecurityNumber")
                    sqlBuilder.Append(" ORDER BY SOCIAL_SECURITY_NUMBER ");
                else
                    sqlBuilder.Append(" ORDER BY LAST_NAME ");

                if (sortAscendingDescending == "DESC")
                {
                    sqlBuilder.Append(" DESC ");
                }
                
                sqlString = sqlBuilder.ToString();

                patientCommand.CommandText = sqlString;

                SqlDataAdapter sqlAdapter = new SqlDataAdapter(patientCommand);

                sqlAdapter.Fill(patientData, "Patients");

                connection.Close();

                returnErrorMessage = "";
                returnStatus = true;

                return patientData.Tables[0];
            }
            catch (Exception ex)
            {
                returnStatus = false;
                returnErrorMessage = ex.Message;
                DataTable patientData = new DataTable();
                return patientData;
            }

        }

        /// <summary>
        /// Create Database Connection
        /// </summary>
        /// <returns></returns>
        public SqlConnection CreateConnection(out bool returnStatus, out string returnErrorMessage)
        {

            string connectionString;
            SqlConnection connection;

            try
            {
               
                    connectionString = System.Configuration.ConfigurationManager.AppSettings["DemoDatabase"];
                    SqlCommand cmd = new SqlCommand();

                   
                    connection = new SqlConnection();
                    connection.ConnectionString = connectionString;
                    connection.Open();                   
                    returnErrorMessage = "";
                    returnStatus = true;

                    return connection;
            }
            catch (Exception ex)
            {
                returnErrorMessage = ex.Message;
                returnStatus = false;
                connection = new SqlConnection();

                return connection;
            }

        }

    }

}
