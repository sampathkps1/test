﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using DataModels;

namespace CodeProjectBLL
{
    public class UtilitiesBLL
    {

        /// <summary>
        /// Convert To String
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ConvertToString(object input)
        {
            if (input == null)
                return String.Empty;

            string newString = Convert.ToString(input);

            return newString;

        }

        /// <summary>
        /// Convert Date to Formatted Date
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string FormatDate(DateTime inDate)
        {

            DateTime dateResult;
            string dateString = UtilitiesBLL.ConvertToString(inDate);

            if (DateTime.TryParse(dateString, out dateResult) == false)
                return String.Empty;

            if (inDate == DateTime.MinValue)
                return String.Empty;

            dateString = inDate.ToString("MM/dd/yyyy");
 
            return dateString;

        }

        /// <summary>
        /// Is Valid Date
        /// </summary>
        /// <param name="dateString"></param>
        /// <returns></returns>
        public static bool IsValidDate(string dateString)
        {
            DateTime dateResult;

            if (dateString == null || dateString == "")
                return true;

            if (DateTime.TryParse(dateString, out dateResult) == false)
                return false;

            return true;

        }

        /// <summary>
        /// Is Date Suppled
        /// </summary>
        /// <param name="dateString"></param>
        /// <returns></returns>
        public static bool IsDateSupplied(string dateString)
        {
            if (dateString == null || dateString == "")
                return false;        

            return true;

        }


        /// <summary>
        /// Set Properties
        /// </summary>
        /// <param name="fromFields"></param>
        /// <param name="fromRecord"></param>
        /// <param name="toRecord"></param>
        public static void SetProperties(PropertyInfo[] fromFields, object fromRecord, object toRecord)
        {

            PropertyInfo fromField = null;
            try
            {
                if (fromFields == null) return;

                for (int f = 0; f < fromFields.Length; f++)
                {
                    fromField = (PropertyInfo)fromFields[f];
                    fromField.SetValue(toRecord, fromField.GetValue(fromRecord, null), null);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Return list of United States
        /// </summary>
        /// <returns></returns>
        public static List<States> GetStates()
        {
            States state;

            List<States> unitedStates = new List<States>();
         
            state = new States();
            state.StateCode = "AL"; state.StateName = "Alabama";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "AK"; state.StateName = "Alaska";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "AZ"; state.StateName = "Arizona";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "AR"; state.StateName = "Arkansas";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "CA"; state.StateName = "California";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "CO"; state.StateName = "Colorado";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "CT"; state.StateName = "Connecticut";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "DE"; state.StateName = "Delaware";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "DC"; state.StateName = "District of Columbia";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "FL"; state.StateName = "Florida";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "GA"; state.StateName = "Georgia";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "HI"; state.StateName = "Hawaii";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "ID"; state.StateName = "Idaho";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "IL"; state.StateName = "Illinois";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "IN"; state.StateName = "Indiana";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "IA"; state.StateName = "Iowa";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "KS"; state.StateName = "Kansas";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "KY"; state.StateName = "Kentucky";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "LA"; state.StateName = "Louisiana";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "ME"; state.StateName = "Maine";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "MD"; state.StateName = "Maryland";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "MA"; state.StateName = "Massachusetts";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "MI"; state.StateName = "Michigan";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "MN"; state.StateName = "Minnesota";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "MS"; state.StateName = "Mississippi";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "MO"; state.StateName = "Missouri";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "MT"; state.StateName = "Montana";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "NE"; state.StateName = "Nebraska";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "NV"; state.StateName = "Nevada";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "NH"; state.StateName = "New Hampshire";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "NJ"; state.StateName = "New Jersey";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "NM"; state.StateName = "New Mexico";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "NY"; state.StateName = "New York";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "NC"; state.StateName = "North Carolina";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "ND"; state.StateName = "North Dakota";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "OH"; state.StateName = "Ohio";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "OK"; state.StateName = "Oklahoma";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "OR"; state.StateName = "Oregon";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "PA"; state.StateName = "Pennsylvania";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "RI"; state.StateName = "Rhode Island";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "SC"; state.StateName = "South Carolina";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "SD"; state.StateName = "South Dakota";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "TN"; state.StateName = "Tennessee";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "TX"; state.StateName = "Texas";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "UT"; state.StateName = "Utah";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "VT"; state.StateName = "Vermont";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "VA"; state.StateName = "Virginia";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "WA"; state.StateName = "Washington";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "WV"; state.StateName = "West Virginia";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "WI"; state.StateName = "Wisconsin";
            unitedStates.Add(state);
            state = new States();
            state.StateCode = "WY"; state.StateName = "Wyoming";
            unitedStates.Add(state);
            state = new States();

            return unitedStates;

        }

    }
}
