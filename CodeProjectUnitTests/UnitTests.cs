﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using MvcContrib.TestHelper;
using MVCWebSite;
using MVCWebSite.Models;
using MVCWebSite.Controllers;
using System.Web.Mvc;
using System.Collections.Specialized;
using System.Configuration;
using DataModels;

namespace CodeProjectUnitTests
{
    /// <summary>
    /// Unit Tests
    /// </summary>
    [TestFixture]
    public class UnitTests
    {
        /// <summary>
        /// Test Display Patient Maintenance
        /// </summary>
        [Test]
        public void Test_DisplayPatientMaintenance()
        {

            TestControllerBuilder builder = new TestControllerBuilder();
                     
            PatientController patientController = builder.CreateController<PatientController>();            
                  
            ViewResult viewResult = (ViewResult)patientController.DisplayPatientMaintenance();
            
            PatientViewModel patientViewModel = (PatientViewModel)viewResult.ViewData.Model;

            List<States> unitedStates = (List<States>)patientController.ViewData["States"];

            string applicationName = Convert.ToString(patientController.ViewData["ApplicationName"]);
                           
            Assert.AreEqual( patientViewModel.CurrentPageNumber , 0 );
            Assert.AreEqual(unitedStates.Count, 51);
            Assert.AreEqual(applicationName, "My CodeProject Application");

        }

        /// <summary>
        /// Test Add Patient
        /// </summary>
        [Test]
        public void Test_AddPatient()
        {
            TestControllerBuilder builder = new TestControllerBuilder();

            string uniquePatientKey = GenerateUniqueID();

            builder.Form["MedicalID"] = uniquePatientKey;
            builder.Form["SocialSecurityNumber"] = uniquePatientKey;
            builder.Form["FirstName"] = "William";
            builder.Form["LastName"] = "Gates";
            builder.Form["AddressLine1"] = "Microsoft Corporation";
            builder.Form["AddressLine2"] = "One Microsoft Way";
            builder.Form["City"] = "Redmond";
            builder.Form["State"] = "WA";
            builder.Form["ZipCode"] = "98052-7329";
            builder.Form["PhoneNumber"] = "(425)882-8080";
            builder.Form["DateOfBirth"] = "10/28/1955";
            builder.Form["PatientDateOfBirth"] = "10/28/1955";
            builder.Form["EffectiveDate"] = "01/01/1975";
            builder.Form["PatientEffectiveDate"] = "01/01/1975";

            PatientController patientController = builder.CreateController<PatientController>();           

            JsonResult jsonResult = (JsonResult)patientController.AddPatient();

            dynamic jsonData = jsonResult.Data; 

            Assert.AreEqual(jsonData.ReturnStatus, true);
            Assert.Greater(jsonData.PatientID, 0);
                            

        }

        /// <summary>
        /// Generate Unique ID
        /// </summary>
        /// <returns></returns>
        public string GenerateUniqueID()
        {

            DateTime currentDate = DateTime.Now;

            string uniquePatientKey = currentDate.Year.ToString() +
                currentDate.Month.ToString() +
                currentDate.Day.ToString() +
                currentDate.Hour.ToString() +
                currentDate.Minute.ToString() +
                currentDate.Second.ToString() +
                currentDate.Millisecond.ToString();

            return uniquePatientKey;

        }


    }

}
