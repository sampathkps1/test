﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModels
{
    /// <summary>
    /// Patient 
    /// </summary>
    public class Patient
    {
      
        public long PatientID { get; set; }    
             
        public string MedicalID { get; set; }   
     
        public string SocialSecurityNumber { get; set; }  
       
        public string FirstName { get; set; }  
               
        public string LastName { get; set; }  
        
        public DateTime DateOfBirth { get; set; }

        public string PatientDateOfBirth { get; set; }

        public DateTime EffectiveDate { get; set; }

        public string PatientEffectiveDate { get; set; }  

        public string AddressLine1 { get; set; }  
              
        public string AddressLine2 { get; set; }  
      
        public string City { get; set; }  
      
        public string State { get; set; }  
             
        public string ZipCode { get; set; }  
        
        public string PhoneNumber { get; set; }
      
    }
    /// <summary>
    /// List of States
    /// </summary>
    public class States
    {       
        public string StateCode { get; set; }
        public string StateName { get; set; }      
    }


}
