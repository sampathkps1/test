﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataModels;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Text;
using CodeProjectBLL;

namespace MVCWebSite.Controllers
{

    /// <summary>
    /// Patient Controller
    /// </summary>
    public class PatientController : Controller
    {
      
        /// <summary>
        /// Display Patient Maintenance Page
        /// </summary>
        /// <returns></returns>
        public ActionResult DisplayPatientMaintenance()
        {
            

            Models.PatientViewModel patientViewModel = new Models.PatientViewModel();
  
            patientViewModel.TotalPages = 0;
            patientViewModel.TotalRows = 0;
            patientViewModel.CurrentPageNumber = 0;

            ViewData.Model = patientViewModel;

            string applicationName = Convert.ToString( System.Configuration.ConfigurationManager.AppSettings["ApplicationName"]);

            ViewData["ApplicationName"] = applicationName;
            ViewData["States"] = UtilitiesBLL.GetStates();
            
            return View("PatientMaintenance");
        }

        /// <summary>
        /// Patient Search
        /// </summary>
        /// <returns></returns>
        public PartialViewResult PatientSearch()
        {
            long totalRows;
            long totalPages;
            bool returnStatus;
            string returnErrorMessage;

            PatientBLL patientBLL = new PatientBLL();

            Models.PatientViewModel patientViewModel = new Models.PatientViewModel();
           
            this.UpdateModel(patientViewModel);

            List<Patient> patients = patientBLL.PatientSearch(
                patientViewModel,
                patientViewModel.CurrentPageNumber, 
                patientViewModel.PageSize,
                patientViewModel.SortBy,
                patientViewModel.SortAscendingDescending, 
                out totalRows,
                out totalPages,
                out returnStatus,
                out returnErrorMessage);

            ViewData["patients"] = patients;

            patientViewModel.TotalPages = totalPages;
            patientViewModel.TotalRows = totalRows;

            ViewData.Model = patientViewModel;

            return PartialView("PatientSearchResults");

        }
     
        /// <summary>
        /// Add Patient
        /// </summary>
        /// <returns></returns>
        public JsonResult AddPatient()
        {

            bool returnStatus;
            string returnErrorMessage;

            List<string> returnMessage;

            PatientBLL patientBLL = new PatientBLL();

            Models.PatientViewModel patientViewModel = new Models.PatientViewModel();          
            
            this.TryUpdateModel(patientViewModel);

            Patient patient = patientBLL.AddPatient(
                patientViewModel, 
                out returnMessage, 
                out returnStatus, 
                out returnErrorMessage);

            patientViewModel.UpdateViewModel(patient, typeof(Patient).GetProperties());

            patientViewModel.ReturnMessage = returnMessage;
            patientViewModel.ReturnStatus = returnStatus;
  
            return Json(patientViewModel);

        }

        /// <summary>
        /// Update Patient
        /// </summary>
        /// <returns></returns>
        public JsonResult UpdatePatient()
        {

            bool returnStatus;
         
            List<string> returnMessage;

            PatientBLL patientBLL = new PatientBLL();

            Models.PatientViewModel patientViewModel = new Models.PatientViewModel();

            this.TryUpdateModel(patientViewModel);

            string dateOfBirth = patientViewModel.PatientDateOfBirth;

            Patient patient = patientBLL.UpdatePatient(
                patientViewModel, 
                out returnStatus,               
                out returnMessage);

            patientViewModel.UpdateViewModel(patient, typeof(Patient).GetProperties());

            patientViewModel.ReturnMessage = returnMessage;
            patientViewModel.ReturnStatus = returnStatus;
            patientViewModel.PatientDateOfBirth = UtilitiesBLL.FormatDate(patientViewModel.DateOfBirth);

            return Json(patientViewModel);

        }

        /// <summary>
        /// Get Patient Information
        /// </summary>
        /// <returns></returns>
        public JsonResult GetPatientInformation()
        {

            bool returnStatus;
            string returnErrorMessage;
            List<string> returnMessage;

            PatientBLL patientBLL = new PatientBLL();

            Models.PatientViewModel patientViewModel = new Models.PatientViewModel();

            this.TryUpdateModel(patientViewModel);

            Patient patient = patientBLL.GetPatientInformation(
                patientViewModel.PatientID, 
                out returnStatus, 
                out returnErrorMessage,
                out returnMessage);

            patientViewModel.UpdateViewModel(patient, typeof(Patient).GetProperties());

            patientViewModel.ReturnMessage = returnMessage;
            patientViewModel.ReturnStatus = returnStatus;
            patientViewModel.PatientDateOfBirth = UtilitiesBLL.FormatDate(patientViewModel.DateOfBirth);
            patientViewModel.PatientEffectiveDate = UtilitiesBLL.FormatDate(patientViewModel.EffectiveDate);
          
            return Json(patientViewModel);

        }

        private void LoadTestData()
        {
            Patient patientModel = new Patient();
       
            SqlConnection demoConnection = new SqlConnection();
            demoConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DemoDatabase"].ToString();
            demoConnection.Open();

            SqlConnection arvidaConnection = new SqlConnection();
            arvidaConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ArvidaDatabase"].ToString();
            arvidaConnection.Open();

            String sql;

            sql = "SELECT * FROM PATIENT";

            SqlCommand arvidaCommand = new SqlCommand();
            arvidaCommand.CommandText = sql;
            arvidaCommand.Connection = arvidaConnection;

            SqlDataAdapter adapter = new SqlDataAdapter(arvidaCommand);

            DataSet testData = new DataSet();
            adapter.Fill(testData, "Patients");

            SqlCommand demoCommand = new SqlCommand();
            demoCommand.Connection = demoConnection;

            demoCommand.CommandText = "DELETE FROM PATIENT";
            demoCommand.ExecuteNonQuery();

            for (int i = 0; i < testData.Tables[0].Rows.Count - 1; i++)
            {
                StringBuilder oSql = new StringBuilder();

                oSql.Append("INSERT INTO PATIENT ");
                oSql.Append("(Medical_id,Social_security_number,first_name,last_name,date_of_birth,phone_number,address_line1,");
                oSql.Append("address_line2,city,state,zip_code) values (");

                DataRow oDataRow = testData.Tables[0].Rows[i];

                String lastName;
                String medicalNumber;

                lastName = Convert.ToString(oDataRow["Last_Name"]).Trim();
                medicalNumber = Convert.ToString(oDataRow["Medical_Number"]).Trim();

                int result;
                int.TryParse(medicalNumber, out result);

                string firstName = Convert.ToString(oDataRow["First_Name"]).Trim();
      
                if (lastName.Contains(" ") == false && medicalNumber.Length == 6 && firstName.Length>4
                    && result>0)
                {

                    oSql.Append("'" + Convert.ToString(oDataRow["Medical_Number"]) + "',");
                    oSql.Append("'" + medicalNumber.Substring(3, 3) + Convert.ToString(oDataRow["Medical_Number"]) + "',");
                    oSql.Append("'" + Convert.ToString(oDataRow["First_Name"]).ToUpper().Replace("'", "''") + "',");
                    oSql.Append("'" + Convert.ToString(oDataRow["Last_Name"]).ToUpper().Replace("'", "''") + "',");
                    oSql.Append("NULL,");
                    oSql.Append("NULL,");
                    oSql.Append("NULL,");
                    oSql.Append("NULL,");
                    oSql.Append("NULL,");
                    oSql.Append("NULL,");
                    oSql.Append("NULL)");

                    sql = oSql.ToString();

                    demoCommand.CommandText = sql;
                    demoCommand.ExecuteNonQuery();
                }

            }

            demoConnection.Close();
            arvidaConnection.Close();

        }

    }
}
