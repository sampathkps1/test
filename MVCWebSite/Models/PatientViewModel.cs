﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModels;
using System.Reflection;
using CodeProjectBLL;

namespace MVCWebSite.Models
{
    public class PatientViewModel : DataModels.Patient
    {
        
        public long CurrentPageNumber { get; set; }     
  
        public long PageSize { get; set; }      
 
        public long TotalPages { get; set; }

        public long TotalRows { get; set; }

        public List<string> ReturnMessage { get; set; }

        public bool ReturnStatus { get; set; }

        public string SortBy { get; set; }

        public string SortAscendingDescending { get; set; }

        /// <summary>
        /// Update View Model
        /// </summary>
        /// <param name="fromRecord"></param>
        /// <param name="fromFields"></param>
        public void UpdateViewModel(object fromRecord, PropertyInfo[] fromFields)
        {
            UtilitiesBLL.SetProperties(fromFields, fromRecord, this);
        }
      
    }    

}