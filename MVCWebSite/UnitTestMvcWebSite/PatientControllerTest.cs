﻿using MVCWebSite.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Web.Mvc;
using MvcContrib.TestHelper;
using MVCWebSite.Models;
using DataModels;
using System.Collections.Generic;

namespace UnitTestMvcWebSite
{
    
    
    /// <summary>
    ///This is a test class for PatientControllerTest and is intended
    ///to contain all PatientControllerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PatientControllerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for DisplayPatientMaintenance
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\Patientmaintenance\\MVCWebSite", "/")]
        [UrlToTest("http://localhost:1327/")]
        public void DisplayPatientMaintenanceTest()
        {
            //PatientController target = new PatientController(); // TODO: Initialize to an appropriate value
            //ActionResult expected = null; // TODO: Initialize to an appropriate value
            //ActionResult actual;
            //actual = target.DisplayPatientMaintenance();
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");

            TestControllerBuilder builder = new TestControllerBuilder();

            PatientController patientController = builder.CreateController<PatientController>();

            ViewResult viewResult = (ViewResult)patientController.DisplayPatientMaintenance();

            PatientViewModel patientViewModel = (PatientViewModel)viewResult.ViewData.Model;

            List<States> unitedStates = (List<States>)patientController.ViewData["States"];

            string applicationName = Convert.ToString(patientController.ViewData["ApplicationName"]);

            Assert.AreEqual(patientViewModel.CurrentPageNumber, 0);
            Assert.AreEqual(unitedStates.Count, 51);
            Assert.AreEqual(applicationName, "My CodeProject Application");

        }

        /// <summary>
        ///A test for AddPatient
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\Patientmaintenance\\MVCWebSite", "/")]
        [UrlToTest("http://localhost:1327/")]
        public void AddPatientTest()
        {
            //PatientController target = new PatientController(); // TODO: Initialize to an appropriate value
            //JsonResult expected = null; // TODO: Initialize to an appropriate value
            //JsonResult actual;
            //actual = target.AddPatient();
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");

            PatientController target = new PatientController(); // TODO: Initialize to an appropriate value
            
            //JsonResult expected = null; // TODO: Initialize to an appropriate value
            //JsonResult actual;
            //actual = target.AddPatient();
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");

            TestControllerBuilder builder = new TestControllerBuilder();

            string uniquePatientKey = GenerateUniqueID();

            builder.Form["MedicalID"] = uniquePatientKey;
            builder.Form["SocialSecurityNumber"] = uniquePatientKey;
            builder.Form["FirstName"] = "William";
            builder.Form["LastName"] = "Gates";
            builder.Form["AddressLine1"] = "Microsoft Corporation";
            builder.Form["AddressLine2"] = "One Microsoft Way";
            builder.Form["City"] = "Redmond";
            builder.Form["State"] = "WA";
            builder.Form["ZipCode"] = "98052-7329";
            builder.Form["PhoneNumber"] = "(425)882-8080";
            builder.Form["DateOfBirth"] = "10/28/1955";
            builder.Form["PatientDateOfBirth"] = "10/10/1955";
            builder.Form["EffectiveDate"] = "01/01/1975";
            builder.Form["PatientEffectiveDate"] = "01/01/1975";

            PatientController patientController = builder.CreateController<PatientController>();

            JsonResult jsonResult = (JsonResult)patientController.AddPatient();

            dynamic jsonData = jsonResult.Data;

            Assert.AreEqual(jsonData.ReturnStatus, true);
        }

        /// <summary>
        /// Generate Unique ID
        /// </summary>
        /// <returns></returns>
        public string GenerateUniqueID()
        {

            DateTime currentDate = DateTime.Now;

            string uniquePatientKey = currentDate.Year.ToString() +
                currentDate.Month.ToString() +
                currentDate.Day.ToString() +
                currentDate.Hour.ToString() +
                currentDate.Minute.ToString() +
                currentDate.Second.ToString() +
                currentDate.Millisecond.ToString();

            return uniquePatientKey;

        }
    }


}
