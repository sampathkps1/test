﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MVCWebSite.Models.PatientViewModel>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <table style="width:100%">
    <tr>
    <td>
    <h3>Patient Maintenance & Search</h3>
    </td>
    <td style="text-align:right">
    <h3><%=ViewData["ApplicationName"]%></h3>
    </td>
    </tr>
    </table>    
   
    <% Html.RenderPartial("PatientSearch",Model); %>
    
     <div>
                                    
        <script language="javascript" type="text/javascript">

            $(pageReady);           

            function pageReady() {

                $.ajaxSetup({ cache: false });

                formIsDisabled = false;

                $(function () {
                    $("#PatientMaintenanceForm #DateOfBirth").mask("99/99/9999");
                    $("#PatientMaintenanceForm #EffectiveDate").mask("99/99/9999");
                    $("#PatientMaintenanceForm #PhoneNumber").mask("(999) 999-9999");
                    $("#PatientMaintenanceForm #SocialSecurityNumber").mask("999-99-9999");
                });

                $(function () {                    

                    $("#PatientMaintenanceForm #EffectiveDate").datepicker({ 
                        showOn: 'button', 
                        buttonImage: '../../content/cal.gif', 
                        buttonImageOnly: true });
                 
                });
                                                                
            }

            function SavePatient() {

                $("#PatientMaintenanceForm #PatientDateOfBirth").val($("#PatientMaintenanceForm #DateOfBirth").val());
                $("#PatientMaintenanceForm #PatientEffectiveDate").val($("#PatientMaintenanceForm #EffectiveDate").val());

                formData = $("#PatientMaintenanceForm").serialize();

                patientID = $("#PatientMaintenanceForm #PatientID").val();

                DisableForm();

                setTimeout(PostChangesToServer, 1000);

            }

            function PostChangesToServer() {
                             
                if (patientID == "") {
                    $.post("/Patient/AddPatient", formData, function (data, textStatus) {
                        PatientUpdateComplete(data);
                    }, "json");
                }
                else {
                    $.post("/Patient/UpdatePatient", formData, function (data, textStatus) {
                        PatientUpdateComplete(data);
                    }, "json");
                }

            }

            jQuery.fn.scrollIntoView = function () {
                var el = this.get(0);
                if (el.scrollIntoView) {
                    el.scrollIntoView();
                }
                return this;
            }; 
 

            function PatientUpdateComplete(jsonPatient) {

                EnableForm();

                firstName = $("#PatientMaintenanceForm #FirstName");
                firstName.scrollIntoView();

                if (jsonPatient.ReturnStatus == true) {
                    $("#PatientMaintenanceForm #FirstName").val(jsonPatient.FirstName);
                    $("#PatientMaintenanceForm #LastName").val(jsonPatient.LastName);
                    $("#PatientMaintenanceForm #AddressLine1").val(jsonPatient.AddressLine1);
                    $("#PatientMaintenanceForm #AddressLine2").val(jsonPatient.AddressLine2);
                    $("#PatientMaintenanceForm #City").val(jsonPatient.City);
                    $("#PatientMaintenanceForm #State").val(jsonPatient.State);
                    $("#PatientMaintenanceForm #ZipCode").val(jsonPatient.ZipCode);
                    $("#PatientMaintenanceForm #PhoneNumber").val(jsonPatient.PhoneNumber);
                    $("#PatientMaintenanceForm #SocialSecurityNumber").val(jsonPatient.SocialSecurityNumber);
                    $("#PatientMaintenanceForm #MedicalID").val(jsonPatient.MedicalID);
                    $("#PatientMaintenanceForm #DateOfBirth").val(jsonPatient.PatientDateOfBirth);
                    $("#PatientMaintenanceForm #EffectiveDate").val(jsonPatient.PatientEffectiveDate);
                    $("#PatientMaintenanceForm #PatientID").val(jsonPatient.PatientID);
                }

                var returnMessage = "";

                for ( i=0; i<jsonPatient.ReturnMessage.length; i++ )
                {
                    returnMessage = returnMessage + jsonPatient.ReturnMessage[i] + "<br>";
                }

                if (jsonPatient.ReturnStatus == true) {
                    $("#DivMessage").css("background-color", "#ebeff2");
                    $("#DivMessage").css("border", "solid 1px #9fb8c9");
                    $("#DivMessage").css("color", "#36597f");
                    $("#DivMessage").css("padding", "5 5 5 5"); 
                }
                else {
                    $("#DivMessage").css("background-color", "#f4eded");
                    $("#DivMessage").css("border", "solid 1px #d19090");
                    $("#DivMessage").css("color", "#762933");
                    $("#DivMessage").css("padding", "5 5 5 5");                    
                }

                $("#DivMessage").html(returnMessage);

            }

            function ResetPage() {

                $("#PatientMaintenanceForm").attr("action", '<%= Url.Content("~/Patient/DisplayPatientMaintenance") %>');
                $("#PatientMaintenanceForm").submit();

            }

            function DisableForm() {

                formIsDisabled = true;

                $('#PatientSearchForm :input').attr('disabled', true);
                $('#PatientSearchForm').css("cursor", "wait");

                $('#PatientMaintenanceForm :input').attr('disabled', true);
                $('#PatientMaintenanceForm').css("cursor", "wait");

                
            }

            function EnableForm() {

                formIsDisabled = false;

                $('#PatientSearchForm  :input').removeAttr('disabled');
                $('#PatientSearchForm').css("cursor", "default");

                $('#PatientMaintenanceForm  :input').removeAttr('disabled');
                $('#PatientMaintenanceForm').css("cursor", "default");

            }           

        </script>

        <br />

        <h3>Patient Information</h3>

        <form method="post" action="./" id="PatientMaintenanceForm">
        <table style="background-color: #ebeff2; width: 100%; border:solid 1px #9fb8e9" cellpadding="2" cellspacing="2">   
        <tr><td>&nbsp;</td></tr>  
        <tr>
        <td>Medical ID:</td>
        <td style="width:200px">
        <input name="MedicalID" type="text" id="MedicalID" value="MSFOUNDER" />
        <input name="PatientID" type="hidden" id="PatientID" value="" />
        </td>
        <td>Effective Date:</td>
        <td><input name="EffectiveDate" type="text" id="EffectiveDate" value="01/01/1975" style="margin:0px 5px 0px 0px" />
        <input name="PatientEffectiveDate" type="hidden" id="PatientEffectiveDate" value="" />
        </td><td style="width:40%">&nbsp;</td></tr>  
        <tr>
        <td>First Name:</td>
        <td style="width:200px"><input name="FirstName" type="text" id="FirstName" value="WILLIAM" /></td>      
        <td>Last Name:</td>
        <td><input name="LastName" type="text" id="LastName" value="GATES" /></td>
        <td style="width:40%">&nbsp;</td>
        </tr>  
        <tr>
        <td>SSN:</td>
        <td style="width:200px"><input name="SocialSecurityNumber" type="text" id="SocialSecurityNumber" value="111223333" /></td>       
        <td>Date Of Birth:</td>
        <td><input name="DateOfBirth" type="text" id="DateOfBirth" value="10/28/1955" />
        <input name="PatientDateOfBirth" type="hidden" id="PatientDateOfBirth" value="" /></td>
        </tr>          
        <tr>
        <td>Address Line 1:</td>
        <td style="width:200px"><input name="AddressLine1" type="text" id="AddressLine1" value="MICROSOFT" />
        </td>        
        <td>Address Line 2:</td>
        <td><input name="AddressLine2" type="text" id="AddressLine2" value="ONE MICROSOFT WAY" /></td>
        </tr>  
        <tr>
        <td>City:</td>
        <td><input name="City" type="text" id="City" value="REDMOND" /></td>        
        <td>State:</td>
        <td>
            <select id="State" name="State">
                <option value=""></option>
            <% foreach (var state in (IEnumerable<DataModels.States>)ViewData["States"])
               { %>
                <option value="<%=Html.Encode(state.StateCode)%>"><%=Html.Encode(state.StateName).ToUpper()%></option>
            <% } %>
            </select>
            </td>
        </tr>  
        <tr>
        <td>Zip Code:</td>
        <td><input name="ZipCode" type="text" id="ZipCode" value="98052-7329" /></td>       
        <td>Phone Number:</td>
        <td><input name="PhoneNumber" type="text" id="PhoneNumber" value="(425)882-8080" /></td>
        </tr>  
        <tr>
        <td>&nbsp;</td>         
        </tr> 
        </table>

        <table style="background-color:#D3DCE5; margin-top:10px; width:100% ">
        <tr>
        <td>
        &nbsp;
        <input id="btnSave" type="button" onclick="SavePatient();" value="Save" />    
        &nbsp;
        <input id="btnReset" type="button" value="Reset" onclick="ResetPage();" />    
        </td>
        </tr>                           
        </table>
       
        <br />

        <div id="DivMessage">&nbsp;</div>
       
    </form>
    </div>

</asp:Content>
