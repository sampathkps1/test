﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MVCWebSite.Models.PatientViewModel>" %>
<div>

    <script language="javascript" type="text/javascript">

    function PatientSearch() {
             
        $("#PatientSearchForm #CurrentPageNumber").val("1");       
        PostSearchForm();

    }

    function NavigateToPage(action) {

        if (formIsDisabled == false) {
            
            currentPage = $("#PatientSearchForm #CurrentPageNumber").val();
            currentPage = parseFloat(currentPage);

            if (action=="next")
                currentPage = currentPage + 1;
            else if (action=="prev")
                currentPage = currentPage - 1;
            else if (action=="last")
                currentPage = $("#PatientSearchForm #TotalPages").val();
            else if (action=="first")
                currentPage = 1;

            $("#PatientSearchForm #CurrentPageNumber").val(currentPage);

            PostSearchForm();

        }

    }

    function NextPage() {
        NavigateToPage("next");                                                                      
    }

    function PrevPage() {
        NavigateToPage("prev");            
    }

    function LastPage() {
        NavigateToPage("last");        
    }

    function FirstPage() {
        NavigateToPage("first");
    }

    function SortGrid(sortBy) {

        if (formIsDisabled == false) {

            currentSortBy = $("#PatientSearchForm #SortBy").val();
            currentSortAsc = $("#PatientSearchForm #SortAscendingDescending").val();

            if (currentSortBy == sortBy) {
                if (currentSortAsc == "ASC")
                    currentSortAsc = "DESC";
                else
                    currentSortAsc = "ASC";
            }
            else {
                currentSortAsc = "ASC";
            }

            $("#PatientSearchForm #SortAscendingDescending").val(currentSortAsc);
            $("#PatientSearchForm #SortBy").val(sortBy);

            PostSearchForm();

        }

    }

    function PostSearchForm() {

        formData = $("#PatientSearchForm").serialize();

        DisableForm();

        $("#FirstPageLinkDisabled").css("visibility", "visible");
        $("#FirstPageLinkDisabled").css("display", "block");
        $("#FirstPageLink").css("visibility", "hidden");
        $("#FirstPageLink").css("display", "none");

        $("#NextPageLinkDisabled").css("visibility", "visible");
        $("#NextPageLinkDisabled").css("display", "block");
        $("#NextPageLink").css("visibility", "hidden");
        $("#NextPageLink").css("display", "none");

        $("#PrevPageLinkDisabled").css("visibility", "visible");
        $("#PrevPageLinkDisabled").css("display", "block");
        $("#PrevPageLink").css("visibility", "hidden");
        $("#PrevPageLink").css("display", "none");

        $("#LastPageLinkDisabled").css("visibility", "visible");
        $("#LastPageLinkDisabled").css("display", "block");
        $("#LastPageLink").css("visibility", "hidden");
        $("#LastPageLink").css("display", "none");

        DisableGridSorting();

        setTimeout(PostSearchToServer, 1000);

    }

    function DisableGridSorting() {

        $("#SortByFirstNameDisabled").css("visibility", "visible");
        $("#SortByFirstNameDisabled").css("display", "block");
        $("#SortByFirstName").css("visibility", "hidden");
        $("#SortByFirstName").css("display", "none");

        $("#SortByLastNameDisabled").css("visibility", "visible");
        $("#SortByLastNameDisabled").css("display", "block");
        $("#SortByLastName").css("visibility", "hidden");
        $("#SortByLastName").css("display", "none");

        $("#SortBySSNDisabled").css("visibility", "visible");
        $("#SortBySSNDisabled").css("display", "block");
        $("#SortBySSN").css("visibility", "hidden");
        $("#SortBySSN").css("display", "none");

        $("#SortByMedicalIDDisabled").css("visibility", "visible");
        $("#SortByMedicalIDDisabled").css("display", "block");
        $("#SortByMedicalID").css("visibility", "hidden");
        $("#SortByMedicalID").css("display", "none");

    }

    function EnableGridSorting() {

        $("#SortByFirstName").css("visibility", "visible");
        $("#SortByFirstName").css("display", "block");
        $("#SortByFirstNameDisabled").css("visibility", "hidden");
        $("#SortByFirstNameDisabled").css("display", "none");

        $("#SortByLastName").css("visibility", "visible");
        $("#SortByLastName").css("display", "block");
        $("#SortByLastNameDisabled").css("visibility", "hidden");
        $("#SortByLastNameDisabled").css("display", "none");

        $("#SortBySSN").css("visibility", "visible");
        $("#SortBySSN").css("display", "block");
        $("#SortBySSNDisabled").css("visibility", "hidden");
        $("#SortBySSNDisabled").css("display", "none");

        $("#SortByMedicalID").css("visibility", "visible");
        $("#SortByMedicalID").css("display", "block");
        $("#SortByMedicalIDDisabled").css("visibility", "hidden");
        $("#SortByMedicalIDDisabled").css("display", "none");

    }

    function PostSearchToServer() {

        $.post('/Patient/PatientSearch', formData, function (returnHtml) {
            
            $("#DivPatientSearchResults").html(returnHtml);
            
            currentPageNumber = $("#PatientSearchForm #CurrentPageNumber").val();
            totalPages = $("#PatientSearchForm #TotalPages").val();
            
            if (totalPages > 0) 
                SetPagingLinks(currentPageNumber, totalPages);
                            
            EnableForm();

        });
    }

    function SetPagingLinks(currentPageNumber, totalPages) {

        $("#PagingLinks").css("visibility", "visible");
        $("#PagingLinks").css("display", "block");

        currentPageNumber = parseFloat(currentPageNumber);
        totalPages = parseFloat(totalPages);      

        if (currentPageNumber == 1) {

            $("#FirstPageLink").css("visibility", "hidden");
            $("#FirstPageLink").css("display", "none");
            $("#PrevPageLink").css("visibility", "hidden");
            $("#PrevPageLink").css("display", "none");

            $("#FirstPageLinkDisabled").css("visibility", "visible");
            $("#FirstPageLinkDisabled").css("display", "block");
            $("#PrevPageLinkDisabled").css("visibility", "visible");
            $("#PrevPageLinkDisabled").css("display", "block");

        }

        if (currentPageNumber > 1) {

            $("#FirstPageLink").css("visibility", "visible");
            $("#FirstPageLink").css("display", "block");
            $("#PrevPageLink").css("visibility", "visible");
            $("#PrevPageLink").css("display", "block");

            $("#FirstPageLinkDisabled").css("visibility", "hidden");
            $("#FirstPageLinkDisabled").css("display", "none");
            $("#PrevPageLinkDisabled").css("visibility", "hidden");
            $("#PrevPageLinkDisabled").css("display", "none");

        }

        if (currentPageNumber < totalPages && totalPages > 0) {

            $("#NextPageLink").css("visibility", "visible");
            $("#NextPageLink").css("display", "block");
            $("#NextPageLinkDisabled").css("visibility", "hidden");
            $("#NextPageLinkDisabled").css("display", "none");

            $("#LastPageLink").css("visibility", "visible");
            $("#LastPageLink").css("display", "block");
            $("#LastPageLinkDisabled").css("visibility", "hidden");
            $("#LastPageLinkDisabled").css("display", "none");

        }

        if (currentPageNumber == totalPages && totalPages > 0) {

            $("#NextPageLink").css("visibility", "hidden");
            $("#NextPageLink").css("display", "none");
            $("#NextPageLinkDisabled").css("visibility", "visible");
            $("#NextPageLinkDisabled").css("display", "block");

            $("#LastPageLink").css("visibility", "hidden");
            $("#LastPageLink").css("display", "none");
            $("#LastPageLinkDisabled").css("visibility", "visible");
            $("#LastPageLinkDisabled").css("display", "block");

        }

        EnableGridSorting();

    }
    
    function PatientSearchComplete()
    {       
        
    }

    </script>
    
      <% using (Ajax.BeginForm("PatientSearch", null,
            new AjaxOptions { UpdateTargetId = "DivPatientSearchResults", OnComplete="PatientSearchComplete" },
                new { id = "PatientSearchForm" })) { %>

    <table style="background-color: #ebeff2;  width: 100%; border:solid 1px #9fb8e9" cellspacing="2" cellpadding="2">
        <tr>
            <td style="width: 10%">
                Medical ID
            </td>
            <td style="width: 10%">
                SSN
            </td>
            <td style="width: 10%">
                Last Name
            </td>
            <td style="width: 10%">
                       First Name
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
                <input style="width: 100px" value='<%=Model.MedicalID%>' name="MedicalID" type="text"
                    id="SearchMedicalID" />
            </td>
            <td style="width: 10%">
                <input style="width: 100px" value='<%=Model.SocialSecurityNumber%>' name="SocialSecurityNumber"
                    type="text" id="SearchSocialSecurityNumber" />
            </td>
            <td style="width: 10%">
                <input style="width: 100px" value='<%=Model.LastName%>' name="LastName" type="text"
                    id="SearchLastName" />
            </td>
            <td style="width: 10%">
                <input style="width: 100px" value='<%=Model.FirstName%>' name="FirstName" type="text"
                    id="SearchFirstName" />
            </td>
            <td style="width: 60%; text-align: left">
                <input id="btnSearch" onclick="PatientSearch();" type="submit" value="Search" />
            </td>
        </tr>
    </table>

    <div id="DivPatientSearchResults">    
     <% Html.RenderPartial("PatientSearchResults",Model); %> 
    </div>

    <% } %>
       
  

 

</div>
