﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MVCWebSite.Models.PatientViewModel>" %>

    <input name="CurrentPageNumber" type="hidden" id="CurrentPageNumber" value="<%=Model.CurrentPageNumber%>" />
    <input name="TotalPages" type="hidden" id="TotalPages" value="<%=Model.TotalPages%>" />     
    <input name="SortBy" type="hidden" id="SortBy" value="<%=Model.SortBy%>" />     
    <input name="SortAscendingDescending" type="hidden" id="SortAscendingDescending" value="<%=Model.SortAscendingDescending%>" />     

    <input name="PageSize" type="hidden" id="PageSize" value="9" />

     <script language="javascript" type="text/javascript">

         function GetPatientDetails(patientID) {

             if (formIsDisabled == false) {
              
                 DisableForm();

                 formData = "PatientID=" + patientID;

                 setTimeout(PostPatientIDToServer, 1000);

             }
                      
         }

         function PostPatientIDToServer() {

             $.post("/Patient/GetPatientInformation", formData, function (data, textStatus) {
                 PatientUpdateComplete(data);
             }, "json");

         }

          

    </script>

<% if (ViewData["patients"] != null) { %>

   <% if (Model.TotalRows == 0)
      { %>

        <table style="width: 100%" cellspacing="0" cellpadding="0">  
        <tr>
        <td>&nbsp;</td>
        </tr>
        <tr>
        <td><b>No matches found.</b>
        </td>        
        </tr>
        </table>

   <% }
      else
      { %>

   <table style="width: 100%" cellspacing="0" cellpadding="0">
        <thead>
            <tr style="height: 25px; color: Black; padding:10px 10px 10px 10px">
                <td colspan="2">
                    &nbsp;<%=Html.Encode(Model.TotalRows)%>&nbsp;patients found
                </td>
                <td colspan="3" style="text-align: right">
                    &nbsp;Page&nbsp;<%=Html.Encode(Model.CurrentPageNumber)%>
                    of&nbsp;<%=Html.Encode(Model.TotalPages)%>
                </td>
            </tr>
            <tr style="background-color: ThreeDShadow; color: White; height: 25px; ">
                <th style="text-align: left">                   
                    <div style="float:left; color:Black;" id="SortByMedicalIDDisabled">Medical ID</div>
                    <div style="float:left;" id="SortByMedicalID"><a href="javascript:SortGrid('MedicalID')">Medical ID</a></div>
                </th>
                <th style="text-align: left">                    
                    <div style="float:left; color:Black;" id="SortBySSNDisabled">SSN</div>
                    <div style="float:left;" id="SortBySSN"><a href="javascript:SortGrid('SocialSecurityNumber')">SSN</a></div>
                </th>
                <th style="text-align: left">             
                    <div style="float:left; color:Black;" id="SortByLastNameDisabled">Last Name</div>     
                    <div style="float:left;" id="SortByLastName"><a href="javascript:SortGrid('LastName')">Last Name</a></div>
                </th>
                <th style="text-align: left">                   
                    <div style="float:left; color:Black;" id="SortByFirstNameDisabled">First Name</div> 
                    <div style="float:left;" id="SortByFirstName"><a href="javascript:SortGrid('FirstName')">First Name</a></div>
                </th>
                <th style="text-align: left">
                    &nbsp;
                </th>
            </tr>
        </thead>
        <tbody>
            <% 
       int i = 0;
       foreach (var patient in (IEnumerable<DataModels.Patient>)ViewData["patients"])
       { %>
                              
                <tr style="height:25px; color:Black; background-color:<%= i++ % 2 == 0 ? "#D3DCE5"  : "#ebeff2" %>">  

                <td style="width: 20%">
                    <%= Html.Encode(patient.MedicalID)%>
                </td>
                <td style="width: 20%">
                    <%= Html.Encode(patient.SocialSecurityNumber)%>
                </td>
                <td style="width: 20%">
                    <a href="javascript:GetPatientDetails('<%= Html.Encode(patient.PatientID)%>');">
                    <%= Html.Encode(patient.LastName)%></a>                   
                </td>
                <td style="width: 20%">
                    <%= Html.Encode(patient.FirstName)%>
                </td>
                <td style="width: 40%">
                    &nbsp;
                </td>
            </tr>
            <% } %>
        </tbody>
        <tfoot>           
            <tr>
                <td colspan="5" style="height:25px; width: 100%; ">                    
                    <div id="PagingLinks" style="text-align:left; vertical-align:top; float:none; visibility:hidden; display:none; padding: 5px 5px 5px 5px">                                       
                    <div style="float:left;" id="FirstPageLink"><a href="javascript:FirstPage()">First</a>&nbsp;|&nbsp;</div>                     
                    <div style="float:left;" id="FirstPageLinkDisabled">First&nbsp;|&nbsp;</div>          
                    <div style="float:left;" id="PrevPageLink"><a href="javascript:PrevPage()">Prev Page</a>&nbsp;|&nbsp;</div>
                    <div style="float:left;" id="PrevPageLinkDisabled">Prev Page&nbsp;|&nbsp;</div>                            
                    <div style="float:left;" id="NextPageLink"><a href="javascript:NextPage()">Next Page</a>&nbsp;|&nbsp;</div>
                    <div style="float:left;" id="NextPageLinkDisabled">Next Page&nbsp;|&nbsp;</div>  
                    <div style="float:left;" id="LastPageLink"><a href="javascript:LastPage()">Last Page</a></div>
                    <div style="float:left;" id="LastPageLinkDisabled">Last Page</div>                                                                           
                    </div>                                    
                </td>
            </tr>
        </tfoot>
    </table>
    <% } %>

<% } %>